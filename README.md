# Trabalho Prático de Engenharia II
## SandubaNaWeb
> Seja o sistema de vendas SandubaNaWeb, que controla a venda de sanduiches e outros produtos associados (refrigerantes, por exemplo) numa sadwisheria. Utilizando o Junit, faça o teste unitário da classe Pedido que registra dia, hora, códigos dos produtos, preços , endereço de entrega, etc. de um pedido realizado. Faça o teste do caso de uso “Cadastrar Pedido”, utilizando os cenários

