import java.util.Iterator;
import java.util.Vector;
import java.util.Calendar;


public class Pedido {

    private Cliente cliente = new Cliente();
    private Vector<Integer> produtosPedido = new Vector<Integer>();
    Iterator p = produtosPedido.iterator();
    private Vector<Integer> quantProdutos = new Vector<Integer>();
    Iterator q = quantProdutos.iterator();
    //data/hora em que o pedido foi realizado
    Calendar dthr = Calendar.getInstance();
    private double totalPedido;
    //produtos no estoque
    private Produto produtos = new Produto();
    //caso o pedido não possa ser realizado, status = 99
    private int status;

    Pedido() {
        this.totalPedido = 0;
        this.status = 0;
        //inicia os produtos
        setProdutos();
    }
    Pedido(String nome, String endereco, long telefone, Calendar dthr) {
        //adicionando cliente
        Cliente cliente = new Cliente(nome, endereco, telefone);
        setCliente(cliente);
        setDthr(dthr);
        this.totalPedido = 0;
        this.status = 0;
        //inicia os produtos
        setProdutos();
    }

     //função que verifica se o código  é valido (4 digitos)
    int addProduto(int codigo, int quantidade) {
        int aux = -1;
        if (Integer.toString(codigo).length() != 4) {
            return 91;
        }
        //verifica se os códigos dos produtos estão cadastrados
        for (int j = 0; j < produtos.getCodigos().length; j++) {
            if (codigo == produtos.getUmCodigo(j))
                aux = j;
        }
        if (aux == -1)
            return 92;

        //codigo valido, adicionar ao pedido
        produtosPedido.add(aux);
        quantProdutos.add(quantidade);

        return codigo;
    }

    double cadastrarPedido() {
        //verifica se a data e horario sao validas
        verificaHorario(this.getDthr(), this);

        if ((getStatus() == 61 )||(getStatus() == 32 )||(getStatus() == 31 ))
            return getStatus();

        if (
                //verifica se o pedido não pode ser realizado (erro 99)
                (this.getProdutosPedido().size() == 0) ||
                (this.getCliente().getNome() == "77") ||
                (this.getCliente().getEndereco() == "78") ||
                (this.getCliente().getTelefone() == 79)

        )  {
            this.setStatus(99);
        }


        //calculando valor total
        double total = 0;
        for (int i=0; i<this.getProdutosPedido().size(); i++)
            //bug proposital 2: colocar = no lugar de +=
           total += this.produtos.getUmValor(this.getUmProdutoPedido(i)) * this.quantProdutos.get(i);

        this.setTotalPedido(total);

        if (this.getStatus() == 99) {
            return 99;
        }
        return this.getTotalPedido();
    }

    void verificaHorario(Calendar c, Pedido p) {

        int hora = c.get(Calendar.HOUR_OF_DAY);
        int mes = c.get(Calendar.MONTH);
        int ano = c.get(Calendar.YEAR);

        if (hora < 17 && hora > 0) {
            // erro proposital. aceita pedido 17h00 e 00h00
            p.setStatus(61);
        }

        if ((mes < 10) || (ano < 2018)) {
            p.setStatus(32);
        }
        if((mes > 10) || (ano > 2018)) {
            p.setStatus(31);
        }
    }

    //  set/get

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setTotalPedido(double totalPedido) {
        this.totalPedido = totalPedido;
    }

    public void setDthr(Calendar dthr) {
        this.dthr = dthr;
    }

    public Calendar getDthr() {
        return dthr;
    }

    public double getTotalPedido() {
        return totalPedido;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    void setProdutos() {
        int[]codigos = {1234, 4321, 4598, 9999 };
        double[] valores = {10, 30, 5, 10};

        produtos.setCodigos(codigos);
        getProdutos().setValores(valores);
    }

    public Vector getProdutosPedido() {
        return produtosPedido;
    }


    public int getUmProdutoPedido(int pos) {
        return produtosPedido.get(pos);
    }
    public Produto getProdutos() {
        return produtos;
    }

}
