import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;

public class PedidoTest {

    @Test
    public void testCodigoProduto() {
        Pedido p = new Pedido();
        Assert.assertEquals(1234, p.addProduto(1234, 1));
        Assert.assertEquals(91, p.addProduto(999, 1));
        Assert.assertEquals(4321, p.addProduto(4321, 1));
        Assert.assertEquals(9999, p.addProduto(9999, 1));
        Assert.assertEquals(91, p.addProduto(10000, 1));
        Assert.assertEquals(91, p.addProduto(12345, 1));
        Assert.assertEquals(92, p.addProduto(1111, 1));
    }

    @Test
    public void testDataHora() {

        String nome = "Flavia Nunes";
        String endereco = "Rua dos Inconfidentes, Numero 122";
        long telefone = 35515080;

        Calendar dthr = Calendar.getInstance();
        Pedido p = new Pedido(nome, endereco,telefone, dthr);
        p.addProduto(4598, 1);

//        dthr.set(2018,10,17,17,01);
//        p.setDthr(dthr);
//        Assert.assertEquals(5,p.cadastrarPedido(),0.1);
////
//        dthr.set(2018,12,16,18,21);
//        p.setDthr(dthr);
//        Assert.assertEquals(31,p.cadastrarPedido(),0.1);

//        dthr.set(1920,10,17,18,21);
//        p.setDthr(dthr);
//        Assert.assertEquals(32,p.cadastrarPedido(),0.1);
//
//        dthr.set(2018,10,17,10,10);
//        p.setDthr(dthr);
//        Assert.assertEquals(61,p.cadastrarPedido(),0.1);
//
//        dthr.set(2018,10,17,17,00);
//        p.setDthr(dthr);
//        Assert.assertEquals(5,p.cadastrarPedido(),0.1);
//
//        dthr.set(2018,10,17,17,01);
//        p.setDthr(dthr);
//        Assert.assertEquals(5,p.cadastrarPedido(),0.1);

//        dthr.set(2018,10,17,23,59);
//        p.setDthr(dthr);
//        Assert.assertEquals(5,p.cadastrarPedido(),0.1);
//
//        dthr.set(2018,10,17,20,05);
//        p.setDthr(dthr);
//        Assert.assertEquals(5,p.cadastrarPedido(),0.1);
//
        dthr.set(2018,10,17,00,00);
        p.setDthr(dthr);
        Assert.assertEquals(5,p.cadastrarPedido(),0.1);
    }


    @Test
    public void testCadastrarPedido() {

        String nome = "Flavia Nunes";
        String endereco = "Rua dos Inconfidentes, Numero 122";
        long telefone = 35515080;

        Calendar dthr = Calendar.getInstance();
        dthr.set(2018,10,17,17,01);



        Pedido p = new Pedido(nome, endereco,telefone, dthr);
        Pedido p2 = new Pedido(nome, endereco,telefone, dthr);
        Pedido p3 = new Pedido(nome, endereco,telefone, dthr);


        //pedido 1
        p.addProduto(1234, 2);
        p.addProduto(4321, 1);
        Assert.assertEquals(50, p.cadastrarPedido(),0.1);

        //pedido 2
        p2.addProduto(1234, 1);
        p2.addProduto(102, 1);
        Assert.assertEquals(10, p2.cadastrarPedido(),0.1);

        //pedido3
        p3.addProduto(10000, 1);
        p3.addProduto(102, 1);
        Assert.assertEquals(99, p3.cadastrarPedido(),0.1);

    }

}